<?xml version="1.0" encoding="utf-16" standalone="yes"?>
<ScanConfiguration Version="9.5">
  <SessionManagement Version="1.2">
    <SessionManagementMode>Manual</SessionManagementMode>
    <AllowConcurrentLogins>True</AllowConcurrentLogins>
    <AWSIdentityDetectedInLogin>False</AWSIdentityDetectedInLogin>
    <UseAutomaticABL>False</UseAutomaticABL>
    <inSessionDomTexts>
      <inSessionDomText>Sign Off</inSessionDomText>
      <inSessionDomText>Contact Us</inSessionDomText>
      <inSessionDomText>Feedback</inSessionDomText>
      <inSessionDomText>Search</inSessionDomText>
      <inSessionDomText>MY ACCOUNT</inSessionDomText>
      <inSessionDomText>PERSONAL</inSessionDomText>
      <inSessionDomText>SMALL BUSINESS</inSessionDomText>
      <inSessionDomText>INSIDE ALTORO MUTUAL</inSessionDomText>
      <inSessionDomText>I WANT TO ...</inSessionDomText>
      <inSessionDomText>View Account Summary</inSessionDomText>
      <inSessionDomText>View Recent Transactions</inSessionDomText>
      <inSessionDomText>Transfer Funds</inSessionDomText>
      <inSessionDomText>Trade Stocks</inSessionDomText>
      <inSessionDomText>Search News Articles</inSessionDomText>
      <inSessionDomText>Customize Site Language</inSessionDomText>
      <inSessionDomText>Hello John Smith</inSessionDomText>
      <inSessionDomText>View Account Details:</inSessionDomText>
      <inSessionDomText>800002 Savings</inSessionDomText>
      <inSessionDomText>800003 Checking</inSessionDomText>
      <inSessionDomText>4539082039396288 Credit Card</inSessionDomText>
      <inSessionDomText>Congratulations!</inSessionDomText>
      <inSessionDomText>You have been pre-approved for an Altoro Gold Visa with a credit limit of $10000!</inSessionDomText>
      <inSessionDomText>Privacy Policy</inSessionDomText>
      <inSessionDomText>Security Statement</inSessionDomText>
      <inSessionDomText>Server Status Check</inSessionDomText>
      <inSessionDomText>REST API</inSessionDomText>
      <inSessionDomText>http:\/\/www-142.ibm.com\/software\/products\/us\/en\/subcategory\/SWI10</inSessionDomText>
    </inSessionDomTexts>
    <ValidAblLogin>True</ValidAblLogin>
    <AutomaticLoginValidated>False</AutomaticLoginValidated>
    <RecordedSessionRequests>
      <request scheme="http" host="localhost" path="/altoroj/" port="9080" method="GET" RequestEncoding="28591" SessionRequestType="Login" ordinal="11" ValidationStatus="None" MultiStepTested="true" sequencePlaybackRequired="true">
        <raw encoding="none">GET /altoroj/ HTTP/1.1
Sec-Fetch-Site: none
User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36
Connection: keep-alive
Host: localhost:9080
Upgrade-Insecure-Requests: 1
Sec-Fetch-Mode: navigate
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-User: ?1
Accept-Language: en-US
Sec-Fetch-Dest: document

</raw>
        <response status="200" bodyEncoding="iso-8859-1">
          <body value="UEsDBBQAAAAIAJySNVOSnhO6bwsAALcjAAAEACQAZGF0YQoAIAAAAAAAAQAYAHxxn1HnrtcBfHGfUeeu1wF8cZ9R567XAa1abW/bOBL+nAX2P8wJ2O0dEFt2XnaT1PEhL27r2zQJ4hS9+1TQEm2zoUUtScUxsD/+ZkjKll/i2t2mTSJK1LxxnmeGdH7+6efZP8D/+BUuf/6p9Y9aDS4777u38KFzcd15gFqtTbev764e/3ffgZEdS7j/dHnTvYKoFsefD6/i+PrxGv774fHjDTTrDXjULDPCCpUxGced2wiikbX5WRxPJpP65LCu9DB+fIhfSFaTXg6XNVt5s57aNGo7m5zOl7HMzPkaSc3T01MvIKJJZ5Jlw/OIZxG05362RpylON5rWWElb19Iq7SCj4UtmGzF/ibFojXmlgFpqfE/C/F8Hl2pzPLM1h6nOY8g8aPzyPIXG5PWt5CMmDbcngujaicnx6e1ZgSxlyZF9gQjzQfnUcyczq+xsVPJ64kxEWguzyM3NiPObQQWlQTZbgKJacXB+FZfpVNw08+jMdNDkdWsys+O85e3IVSpeAaRYpjwDa6jpcl9Za0au/kwEakdncHp6S/uXbR1oPTYvTzQ4x5nOhlFgNEYKbw1JONYQktT9cTNqn81OYnYQxmW9SX3ss+jZqPxSwR9pdGU86iBweNS5ixNBa1QGJucJWFMMpwQHa7oq2VT0GqC01DzQdRuMWfjBwyUvsHoYqyX4iuylL94o1piPASjk+rTMRtyE0s1VPWhGETB2IOTQxhxMRzZ85NG3G7FDL+tSxlvUwpMiiHaoGlSBM9hiAvg4+fneetuFIabrFsxTtKTYNwAc6lcIbquTZwBZxgymWJaKan0GeZIiivUQ2XQzVoxTXTmwV+wHIvD12Px7zJxBSIs5V9oiOtZxwyO2ld+AJ/MK4KPVgQPOE/7LHnyrrwLo/J1yfpcAiYUxufFhmRq+9+t2D1te+YJiyyyvLCV7I8gY2O8/rPgeho5Y8IlSxJuzBOfnke9EmZrxZiiPxZ+oQocvld+tl+mcmndpV5KvLXrHdaJvBxqVWRpzWXSWaHlP98sZ9dQs1RgtOtf8+Gbf70NOX/WQOB5KLrLTfnpAfwlFwnJQLclck6ZrIfHR2Wy/uaSdcUbd0VYdJwXE7QdkyA/eKLwQF0C4AJwPSk4cSUOw+ODY4R1IplBPu5b6Gvo99GXkns+ONObwTu60yWfEKhrPc0HX6TCRKqAsXlQutc8KgP/zLUVCZM1tyyEEeKytyEyPZ4UmoPDXRS34desb/K3ZR5fJAmumP0GIEuXBiopkHrbd7c33dsOXF7c/tG9fQ83d1gVPS9QFEPQN4QmSeDV6MyJjIwq7y1Z8E0sIzqNq5WEYmjfdx56d7cXNz/KyMM1Rh7ubGS/MCJDyAYjex8vbm7g8lMPY9vrfYepq3YerbHzaGc7PTF6Quze9rrXHbi4ebx7uIOPnx4/rY1qQNscJu6aWqjO7fVCA7VQnCea5XmlOi+V4r15J/Z4dxW6MZQCvlObVcVqBarGyK/lnBdbeCdownaE10ahyjQdB1UZtPKOj+cVs6HGBvlIqmVr4QP6jaRcyMmK+ELOJGLQ+0xHC0TuOqdyUT/yrFgt968r/pLyXGEb6Q249gO41yotEutXEYVvp+5gC3XJiCdPyKChlobRjooOt1AkFbbHXssNXpYumR1VHW3jE9NpUHVFlzuqON5ChcieubFjvBcUdec34Fc2RvruZqbALUHCd1T/2xbqlR1x7RXf0SX0uH4W2Fus0dWKC+lAvB4eB7vAY4EO17DhjwDJ799WvxEkuy73yRb6JM/SGUhu/GBTzDfpO91C39/M4GZjCx2izM8ygb8vXZsbSG2mS3MrNCd0eGUPs/Gu2jZw2kzbD0TH4S7o2KYI/5AysoFvww6J9VURYn1Bl2F3tEukNzDtt7dhuyia8W0yFHX+wqlAJIx26zuL2sCdwWZP3EpXWVtpeODy+zRuYKugMdczwrynS3hQaryrmg0kVS4G0xzLw4w0aFDRghvDV4XPGAlz3CRa9LnfGPfK4eu48Zeuo5z1fNQ+hr7RPV7p9pT0ByJzcPmeb2+v+r3nBP7BpxOFDHi2cOi1DyqjdhD6LKN+ZX9+UTY0+2DYM/42+7Trph2UCceB2FG6kym/iwwnPI2F7WRjy83k4eEvaw5TQoT6FO1vlvJgZaAr79Wldwa12BG8e+h0oHwgpIR7NgW/IqghOFMqvVXYLWP7gV5zzG2pco6XmOAuLAasgokWlsNQPHOYqgLGCreeVow5PTM51jaMLSBt4jcZ5ibx7Kua1mFZ28o4Qde4rpwMlAcCasyb/iygDOzvVKP8PjlqEtnQUUB4f1XDzmGttJsPnEnoYFjQ7Xciwxo363BXA/iOGVuHnhjnkteppRggZP2ZLnwecVdOKCIIMEBk50zTQmHo+sUUs7AQMt2HvNDJiBkOkmWpDz9Si9XYnUDGJ0BpxvdBcgsLaf3GkMyxQB2ajObeaOo+0C8Ml8x9TgxKN+pwYYDh7KGzEadSidqHCYenTE3cOo6ZfuLW3SucIItWuSdlxUSI4B18PmLP3K+9ZskTik0QHeRdrhU+EXYB8buAYVNmHGzIjIOTjZmBP+OtMbeuvboM9+BK8xSbyEq7tZoc/1PFG1x3JidsakAq5VA6wPV1NzBOmDcuUpgitOhj5Lkprqo/7wHCcJ2k4PzMuqzBoGZ0wMUxT8qX+WAgEsGzZOrWhczXCrPhBdFpuKkjyif7LgsT3EilqNWiTRJqPjnYbF0h8U6RvyULwkCjJQtpV9+Mt2XMbxXi1zs+6ClZVGrtaphxLhMZhXaoVAocsaimnBsQlOtWFcMRWGaeEKcccxbTfMEdFxUsK8JYFySROd9RijAjh9YRChpwhisw0k4aRpwnlihxnZ317876PpZ88czCQrpzPmGJvVedfkTUHTQajYq7arDkGJFOiiuK3REvYWnJcHTKZdwmZvSG+NUIA3fiTZbN3kv5gBXSLrxpgtn+1XJE7yIlIgtKng55SRLYirigu0wkJvHp7ZorpwpbILQUm3F8yvwCZdx74/jN+MNQ587yu0+c55TsaNkAFWVWEC1j6ITLDXoFbRgLw1dSek1ib6Kkw03F6nCBkl6VXwEKbq2e+fTLnwUyusso1199pszM4OT9JYh7TPRblqkSE68D8IoymdtZIpuR0ha8BjQYecDZNUt9SDWbOJJqUkYdN+bKkEpoBYnIaMKE6bAOvqr5NRjnKJ/oavapyWuxXQBJOF6snOTvLczYq57oL398++7u7nHh9HFZ2Ow4cqAQAa+eRjp9Sx8HHW9zNl0FSwnheyWFx4wX7I/p/c+/qoP1en/bQu8i1GZ80aM2oNwt/yDl1FoUxp//hXYfd8m0WXYPwB0FbqEPtlY4YcMh18Fr95F3+6HTe4SL++72en5NVD4Ntw4aB01Y2hd0s6Q+23lQkoQdRioMXmGbu7DLJtL1Av6D7U/fUGeMkMqLPtUKZKX+FLqXHxFzGlHgiYiQRMRmFO4esNPLFTZ6ajAXmvKx6/ZYwCiflxdXlBGEJDMPx1UE0pSXNI5WAMuxVPkd8FzqcyEzrllfSGEFsqfv2LzJyNr4OqLZMWHpRaasaw2xcITdkXvkulshEeIkZ3+uQQxQKDaxyABILEgF2Nxi8s3sRI0xtTpeqfEtMHK1nCIhC2xHiZGJkKtWzMWH6pBCxIitI1ci6GgCaUcjv0wpMGgAoKXYDHHhem1setymWTkmkoKndRe9VPGKbOeqMQXuYkiCFuaJwqrDrp5ccmRWuKXyvBn8qMM7lO12QZVyUwkLki218kNFUpbTfLb9n/9pR615dFAX/XEdW47YqIFF/3hcRjEuTMyzGHfaVMSHSk/j3ucuHdi1/64IV5Ffq0ZXCBv3YXBAEPUaJ/tAENpfTvF9xIQEN9tQvSZeSGdNkOfsyqexMf1lh/9TDwR1eRJApwBzGv8/UEsBAi0AFAAAAAgAnJI1U5KeE7pvCwAAtyMAAAQAJAAAAAAAAAAAAAAAAAAAAGRhdGEKACAAAAAAAAEAGAB8cZ9R567XAXxxn1HnrtcBfHGfUeeu1wFQSwUGAAAAAAEAAQBWAAAAtQsAAAAA" compressedBinaryValue="true" />
          <headers value="HTTP/1.1 200 &#xA;Content-Length: 9143&#xD;&#xA;Set-Cookie: JSESSIONID=C7DC9861991F70ADA043E95F8CFDE664; Path=/altoroj; HttpOnly&#xD;&#xA;Date: Tue, 21 Sep 2021 12:50:33 GMT&#xD;&#xA;Content-Type: text/html;charset=ISO-8859-1&#xD;&#xA;" />
          <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="True" expires="01/01/0001 00:00:00" />
        </response>
        <sessionCookies />
      </request>
      <request scheme="http" host="localhost" path="/altoroj/login.jsp" port="9080" method="GET" RequestEncoding="28591" SessionRequestType="Login" ordinal="12" ValidationStatus="None" MultiStepTested="true" sequencePlaybackRequired="true">
        <raw encoding="none">GET /altoroj/login.jsp HTTP/1.1
Sec-Fetch-Site: same-origin
User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36
Referer: http://localhost:9080/altoroj/
Cookie: JSESSIONID=C7DC9861991F70ADA043E95F8CFDE664
Connection: keep-alive
Host: localhost:9080
Upgrade-Insecure-Requests: 1
Sec-Fetch-Mode: navigate
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-User: ?1
Accept-Language: en-US
Sec-Fetch-Dest: document

</raw>
        <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        <response status="200" bodyEncoding="iso-8859-1">
          <body value="UEsDBBQAAAAIAJySNVNb8LJSEAoAAIcgAAAEACQAZGF0YQoAIAAAAAAAAQAYAHxxn1HnrtcBfHGfUeeu1wF8cZ9R567XAa1ZbXPaSBL+7K3Kf+jV1WadKoPANj6/AFd+S8IdNi5Dam8/uQZpgElGGu1oBOZu779fz4uEwBhDNk7izEgz/XT3dPd0t9799O4nwL/4884O3/3U/LlSgavbT517+Hx7eXP7CJVKWz++6V0Pfn+4hYmKODx8uep2rsGr+P5vR9e+fzO4gX9/Htx1oV6twUCSOGWKiZhw37+998CbKJWc+/5sNqvOjqpCjv3Bo/+sadX1ZjesqNLOaqhCr214MpjPEY/T1hpK9bOzM0vA04vOOYnHLY/GHpjd9k9zQkmI872mYorT9iVXQgq4y1RGeNO3D7UumhFVBDRKhf6RsWnLuxaxorGqDOYJ9SCws5an6LPyNeoFBBMiU6paLBWV09PGWaXugW+pcRZ/g4mko5bnE4P51U/VnNNqkKYeSMpbnpmnE0qVBwpBHG2zQJNp+o755lCEczDLW15E5JjFFSWS80byfOFUFbIpsBDVhDuo9FYWD4VSIjLrYcZCNTmHs7NfzF7kdSRkZDaPZNSnRAYTD1AbE4GPxpo5EuijKUtiVlW/pokmsYc0FBlyamm3vHqt9osHQyGRlZZXQ+VRzhMShkyfkJunCQncXNMwRKQb6Z+mCkGKGS5D5EOv3SSGx8+oKNlF7aKuV/TL4pA+W6aaLBpDKoPy24iMaepzMRbVMRt5jtnD0yOYUDaeqNZpzW83fYL/lDEZy1MIhLMx8iD1Ig+mbooHYPVn11nuugLVrbl7wRzXbxxzI7Sl/IT0uDIzDJyjyniIZiW4kOdoIyGeUB/BoBM3fb3QsAd/wqoujl7XxT9yw2XoYSF90lM8zypasNe+thP4kr5C+PgF4RGl4ZAE36woH90s387JkHJAg0L9PCtnTG37f9M3b9s28rhDZnGSqZL1exCTCMd/ZFTOPcOMG5IgoGn6jc5bXj93s7Vk0mwYMXtQGU4/CbvaHlN+tGYoVwxv7Xm7c9JSjqXI4rBiLOk8k3z/11XrGksSMtR29Wsy/vXDhbP58xo6nnVFM9xkn9aBnxIWaBooNseYkxvrUeM4N9YTY6wvpDEj7Ysm5vnatU0kwfhgA4V11BUHXHJcGxQMudwP3evDBrp1wEmK8XioYChhOERZ8tjz2bBed9LpJx0tEzrqWkmT0RMXaEglZ6wf5uLVj3PFT6lULCC8Yo5F+4iOZRdOM30aZJKC8TvPb8P7eJgmF7kdXwYBnph6wyFzkUYiyDD0tnv33c79LVxd3v+rc/8Juj28FW1c0Fp0St+gmiCAV7WzCGSaqfzZCgdv+jJ6Z2ruSu3F0H64fez37i+7P4rJozVMHu3M5DBLWYwu65js3112u3D1pY+67fe/g9WXfB6v4fN4Zz5tYLQBsXPf79zcwmV30Hvswd2XwZe1WnXetnATM9Yp1O39zVICtXQ5zyRJktLtvHIV7y0ysUHv2mVjSAVsplbciuUbqKwje5aLuNjEJw4J0xFambhbpm5iUDmClvZYfV4T5e5YRx+Dap5aWIW+YZRLNlkin/GCIip9SKS3FMhN5pQf6h2Ns5fX/evATyFNBKaRloEbO4EHKcIsUPYUkfh2cIdbwAUTGnzDCOruUjfbEehoCyAuMD22KF0c5iKlO0IdbyMTkaGDutbDHSEaW0CweEpTFeEzB9RZPID3JMLw3YnTDEuCgO4If7IFvFATKi1wTw+hT+WUYW6xBqvpZ9w48Xr3ONzFPZbC4Zpo+COc5O9vw290kl2P+3QLPE7jsHCSrp1s0vkmvLMt8P6iBddrW2Cw3D5zA/4+c61vCGoFlqSKSaq9w4I9FvNd0TbEtALtB3rH0S7esc0l/EOukQ3x1lVIZCgyp+tLPXTV0S6a3hBp3y7DdgEq4m0wZlX6TPUFERBdre9MakPsdDzbwC1kOWoLCY+Ufx/ihmjlEBNZBMwHPYRHIaJdYTYEqfwwiKR4PRRBQ09KKFgYvkq8iEho42kg2ZDawrifT1/3Gzs0GWWR8+n00eWNsC7ZE9z2Qxa+ZVM+5FAnmXnmy19NMffM2km93Yt1TghXJNZJiy2gmj6+KBYZngSMKRbWEwp4yHjeQMKIxWCKpwNIOCUpBWfM0GeK9pIUiILjeqPSaDQqJ/XGmZUIKSbtpmbfqPEpULz2ZH+7TtvTHWHxU4TnjPVhIYHthvzt48da7eTkwrRLUvYfel4/TNRFuXtimiclIX2NhbpPFo9MpyvvZoXCVo1FrwuvwqL/wO0rzakbooGbxkLLw4icyRj2UewRk5HpPeybVR8+XCyaWXkVbvpZL3pbpRmg41Opcc9LS/ylNas71jRONK8ZC3MJzND1QFbNod6oudbhVnBLL0t9k7fEekBrnAkZfqdYidtuRTOzXDg3eUuq7Vm3rKxw95K/V1pNlqmhivvLvSdrYK8ztNyFWjRuFp2b3JZt/8bNdGRJyofvfyVTYp9atL1RFhszh5QqUwjvf4D/uh4XABvBfohPTVZhGyFoLlXLNZpLeS3A3pqljuZFsex/QDmGgk37zKGt2WlG9vcy70sOFs21SlalsE8X3Fcx3RyrCbx/D+6VRS2/XJYu92YlM/pSHo3x82soH5YpuVV4UVFVFrB4s1ZxAIRTqWDf+11kEGUYY1FlmHkRE/tDyFxw8JY25WyPCLK5+RyWFLE7fuGG2+GXTnOGN66YVUWMBWwIrcIWL1x8Nga7chMWncrVz1Ife73BUlfFes7Ca4o2y0gI9XqXxYCttLkb2/TcJJuSYJ5nI2YCD4KzYL5ITm370f7+szxZj3uyBW6qO5xMOeC+m0Ff4X2cVwE/CDxFmllq+xoujcHsXxcB5gWYFscWeLA14IyMx1Q6qc2nvPbjbX8Alw+d7XHeByKZu0eHtcM6LH3kO4BOHFTzMqWcJoUsxRGL6FL1MMBcxxL4J8zoEKtjjAIpJNmQs3RCQxjOoXN1B9dCJkKazFd/8DA5Uio4xZX4Al1QjBZEQxph/qD0cky29FI6GlGMclOqyy5ca2gmrgwHTLFCqvQCXI5cAEkStDSDtqA6zXhMJRkyzhSjmHfFYcFySDX9tIryIPe5FLHADA59l3AYusxPv6pi6hYxTqShc7BAwPBH4vkBKIFMM6mDgUTjK/hERB9ld6A4l0Z+yucYvFkcYHaNqSFf5mJBHulMcUkIHkGZU/0ZAFMxvF5nRGJBjUjCMADIaXgAlJmSlD6bugAQmEWoFhpWjfZCQUu0jahpiheQoSBZ+k2rVbpqRYs0F5nU4VWjKM2gk6MKH5F2JFAYFuvYaXaU1OLy3rHQVFbNvChrFp+sK/XjwyobRtVARH4qRgrlo36uRT9LfRr7mEzgAdOxkHO//1tHNyLaf5WEdqKq7gP7bfu7XK5fo9uYj1zOg+CwVjs9AO1CB6smfoA+wcGsTkHfcBgXwmoeum3MLn1l8vUXa/sJG506r3B0dbMI4/8HUEsBAi0AFAAAAAgAnJI1U1vwslIQCgAAhyAAAAQAJAAAAAAAAAAAAAAAAAAAAGRhdGEKACAAAAAAAAEAGAB8cZ9R567XAXxxn1HnrtcBfHGfUeeu1wFQSwUGAAAAAAEAAQBWAAAAVgoAAAAA" compressedBinaryValue="true" />
          <headers value="HTTP/1.1 200 &#xA;Content-Length: 8327&#xD;&#xA;Date: Tue, 21 Sep 2021 12:50:35 GMT&#xD;&#xA;Content-Type: text/html;charset=ISO-8859-1&#xD;&#xA;" />
        </response>
        <sessionCookies>
          <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="True" expires="01/01/0001 00:00:00" />
        </sessionCookies>
      </request>
      <request scheme="http" host="localhost" path="/altoroj/doLogin" port="9080" method="POST" RequestEncoding="28591" SessionRequestType="Login" ordinal="13" ValidationStatus="None" MultiStepTested="true" sequencePlaybackRequired="true">
        <raw encoding="none">POST /altoroj/doLogin HTTP/1.1
Sec-Fetch-Site: same-origin
User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36
Referer: http://localhost:9080/altoroj/login.jsp
Cookie: JSESSIONID=C7DC9861991F70ADA043E95F8CFDE664
Connection: keep-alive
Sec-Fetch-Mode: navigate
Upgrade-Insecure-Requests: 1
Host: localhost:9080
Content-Length: 41
Cache-Control: max-age=0
Sec-Fetch-User: ?1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Origin: http://localhost:9080
Accept-Language: en-US
Sec-Fetch-Dest: document
Content-Type: application/x-www-form-urlencoded

uid=jsmith&amp;passw=demo1234&amp;btnSubmit=Login</raw>
        <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        <parameter name="uid" captureIndex="0" value="jsmith" type="BODY" linkParamType="simplelink" separator="&amp;" operator="=" reportName="uid" />
        <parameter name="passw" captureIndex="0" value="demo1234" type="BODY" linkParamType="simplelink" separator="&amp;" operator="=" reportName="passw" />
        <parameter name="btnSubmit" captureIndex="0" value="Login" type="BODY" linkParamType="simplelink" separator="&amp;" operator="=" reportName="btnSubmit" />
        <response status="302" bodyEncoding="utf-8">
          <body value="UEsDBBQAAAAAAJySNVMAAAAAAAAAAAAAAAAEACQAZGF0YQoAIAAAAAAAAQAYAKWZn1HnrtcBpZmfUeeu1wGlmZ9R567XAVBLAQItABQAAAAAAJySNVMAAAAAAAAAAAAAAAAEACQAAAAAAAAAAAAAAAAAAABkYXRhCgAgAAAAAAABABgApZmfUeeu1wGlmZ9R567XAaWZn1HnrtcBUEsFBgAAAAABAAEAVgAAAEYAAAAAAA==" compressedBinaryValue="true" />
          <headers value="HTTP/1.1 302 &#xA;Location: /altoroj/bank/main.jsp&#xD;&#xA;Content-Length: 0&#xD;&#xA;Set-Cookie: AltoroAccounts=ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=&#xD;&#xA;Date: Tue, 21 Sep 2021 12:50:53 GMT&#xD;&#xA;" />
          <cookie name="AltoroAccounts" value="ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=" path="/altoroj" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        </response>
        <sessionCookies>
          <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="True" expires="01/01/0001 00:00:00" />
        </sessionCookies>
      </request>
      <request scheme="http" host="localhost" path="/altoroj/bank/main.jsp" port="9080" method="GET" RequestEncoding="28591" SessionRequestType="Regular" ordinal="14" ValidationStatus="None" MultiStepTested="true" sequencePlaybackRequired="true">
        <raw encoding="none">GET /altoroj/bank/main.jsp HTTP/1.1
Sec-Fetch-Site: same-origin
User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36
Referer: http://localhost:9080/altoroj/login.jsp
Cookie: JSESSIONID=C7DC9861991F70ADA043E95F8CFDE664; AltoroAccounts=ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=
Connection: keep-alive
Sec-Fetch-Mode: navigate
Upgrade-Insecure-Requests: 1
Host: localhost:9080
Cache-Control: max-age=0
Sec-Fetch-User: ?1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Origin: http://localhost:9080
Accept-Language: en-US
Sec-Fetch-Dest: document

</raw>
        <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj/bank" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        <cookie name="AltoroAccounts" value="ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=" path="/altoroj/bank" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        <response status="200" bodyEncoding="iso-8859-1">
          <body value="UEsDBBQAAAAIAJySNVNsLgjv5AgAAEUXAAAEACQAZGF0YQoAIAAAAAAAAQAYAKfAn1HnrtcBp8CfUeeu1wGnwJ9R567XAa1Ya3PbNhb97MzkP9xwtk13xhL1sFPbkdRxZCVRx7Y8ltxsPnlAEhIRgwQLgJa10x+/Fw9alCxnnd0qiQKQwH2fiwO9fvX6FeBf/Lx2w9evem8aDfgw+jS+hM+j07PRNTQaA/P4bDKcfb0aQaozDlc3H87HQwgaYfilOwzDs9kZ/Ovz7OIc2s0WzCTJFdNM5ISH4egygCDVujgJw+Vy2Vx2m0Iuwtl1+GBktc1mP2zo2s5mopNgYG2yOh8ynqv+Dknt4+NjJyAwi044yRf9gOYB2N3uTy+lJMH5Xk8zzenglGshBVyUuiS8F7qHJha9jGoCRkuD/lmy+34wFLmmuW7MVgUNIHazfqDpgw6N1vcQp0QqqvtMicbR0eFxox1A6KRxlt9BKum8H4TE6vwWKr3itBkrFYCkvB/YuUop1QFoVOJl2wVGTC/0xvcikazALu8HGZELlje0KE4Oi4f3PlQJuweWYJhwB5XB1uJIaC0yux6WLNHpCRwf/2T3oq1zITO7eS6zKSUyTgPAaKQCHy2McSQ2qal7Ylc1v6nCiNhDGZpEnDrZ/aDdav0UQCQkmtIPWhg8ynlBkoSZDPm5Kkjs50aGFSL9yHx6OgEplrgMNXeCQY9YGz9joOQ5RhdjvRVflif0wRnVY9kClIzrbzOyoCrkYiGaCzYPvLGdoy6klC1S3T9qhYNeSPCftiXjbEqAcLZAG6RZFMC9n2ICXPzcOmfducBwG+ueGGf0ltpbN8diqlJkxo2lteAEY8YTrCvBhTzBIkkwRVPUBpP5vBealdZA+Au2o9F9Phq/VaXLEGMJvTVTzGgTazgYDN0EbtQzgg+eCJ5TmkQkvnO+fPSzajsnEeWAJYURetC+nAbu/15o3w5c7/FpZnlR6lr9B5CTDMd/llSuAmuMH5I4pkrd0VU/mFZA2ylGlVHGXKpKnH4SbrVLVJVcO5Rbpbcz4z5RxsuFFGWeNGwtnZSS//J2u74WkiQMo938Vize/vO9r/qTFkLPgdEOv1ehDsK3BYuNDHSbY9epyrV7eFCV6ztbrk+8sSODRtv1QgNu20uwQ7hW4aC6BcEN6Lq2YMVVSPSvO4cI7JgThR050hBJiCL0peo+n63pbe+deTI2PiFUd3pazG+5wEKqwbHdqdxrH1SBv6dSs5jwhk2LAYnpZu99ZKY0LiUFi7wgHMDPeaSK91Udn8YxZkzvhGRE8rswIyy3lVy5NRdxiQ14cPEVTofDyc3lzPUEEz8f7u8EJY7h2bism5gxp3q2pfe/ohhxqew5afALg6vR9XRyeXr+dxnZ3WFk94eNjErFcgSrN3J6cXp+Dh9upuPL0XT6P5j61M6DHXYe/LCdriW6Vji+nI7PRnB6PptcT+DiZnazM6oeZ2uA2LGhT6PLsw3ytHEwLyUpitrJvHUM71kJF6OLD7h7Nhl6LublwCOdeRF47RYf0/pxVQ+qS/66hfbwiTcNuQttpP5Eatt2FW6sHIzhy+nlDO2EZrPZC6P625JXakxoIyKDjUZtuVGVugual9850DcBOviD0SV4QMO0zLCfrlyCUOLLdHR267D807GcmqprGmONOFrrXqofVPf0VF6rm1PpdM38DD7i0VLXsIfU4gclK439VD3KTShM7ZMtqfAy85+e/VaJPY4fCqI9A3SHO1zSpYJT06qR1f5goA53a0IQY6dn/6ZO0bCawpRpbPlI+Es8SHbo6oUlt+eiB4ID7zbGDGAtwvaeIkVwxzzXvc/hxYjEtBhYV72GPwvqvfq/XtoefEbgCvhdpDlMkaKk1dGNrx6XFf5A/0J5LDIKWsDGrQUmuUFo0y4Oi/VGy+MdeUrwMsO4eobHq1QsPY6cmb6trBl7xSlm19t819FzjNcmHM+cwpMaIdmgUpzOvS4jVlFOkXQqTCV2rYrxcaa0l6cc8dt44nbv9URhvKjI3VELPwjrgRvAlNxjV8T6c8tM1nds6FYbujBMaXyHW57fcXDYPW4ddVrd4+7xu87REe7dfgRDJOtMw5DIpCbokXM6h+u3m1181bgc6fwTrZx+pLC449MEv2oZ+C6PraL/WMaGe5iR1XEba966dd/+lnt7gW32tpAiE7iyfps7PG5VhdEaGPkm9720Y64OSHZ1yYltjW9MGXfcOWktqtZ+FSWk5B7Li9IcCkkbeBJKcU8Tc00AtMmX9ye8/MAfTBHUrFMgELugcobRATGHf7RNyt48VTHkLL4zlM82kbcon69My3iLgJO2PxgUuce13Z4nY3owMru49F6NSq+5tIebZ9T1/vJIsrd/U/k4mcw2aIFTsBb/yBPmQujnaYJVtnVDe9o7d5BGye5JvHJE58pN4Epg2OwJ6gQ75uy+/6pPdut99wK9ypBzpr3iqZ/hoUQ0zXDB36hcocxS3cYGztW5JPHqYJWVyuH8BfrgxQqXZLGg0nttf4caXI+mMzi9Gr9cz8+xKFb+UafVaW/2+n0Y53Hzkc/VTp2EKRyxjG7wq1lKvYDfYUkjZY5JpqAoI2ylKSIuWsH4wwUMhSyEtMC1INS4TwnEfFHiC0URbWuhCc0Q39oszxd2KZ3PsZ2xe2o4vkGmkYmYTspYK2A5btFmAS5HKyzw8AJntK2l3pc8p5JEjDPNqMI+kDyanFAjXzXRH7S+8iIXGpuCpHgEGnZgpJtXTWQDGeNEWjn7aw1sjkJX+wb6OmUygYJILL5HO1FjiL57pTiX1n/KV9g3WR4jfc01XrQ2rFiLNz0MlyQQEPRZBbZrCWzqSyKR0qEmYQ0AtDTZB4pvsRrpA3ZAEzQJLMOw0KRpo5cIWpNtXVWqxOPfSJBM3Zmwom0uZ+jSSpQSSpsq456q/GjCR5SdCXSG5aZj2R21sBScEty2EEbKdpn/WpX5+vfWRvug02RR1kQ2Eiox1+gfDasohqUKaR7iEYYJpgshV+H0y7iNl5LB/yvCgKhp7iXhwH1XODCfIcLG/j7jEQSdVutoHwyE9rdLfB8xwcGuVhhDZfpC0qxat+vZtR9IQvNzq/v9FUFd8UZDFtdtfO8/UEsBAi0AFAAAAAgAnJI1U2wuCO/kCAAARRcAAAQAJAAAAAAAAAAAAAAAAAAAAGRhdGEKACAAAAAAAAEAGACnwJ9R567XAafAn1HnrtcBp8CfUeeu1wFQSwUGAAAAAAEAAQBWAAAAKgkAAAAA" compressedBinaryValue="true" />
          <headers value="HTTP/1.1 200 &#xA;Content-Length: 5957&#xD;&#xA;Date: Tue, 21 Sep 2021 12:50:53 GMT&#xD;&#xA;Content-Type: text/html;charset=ISO-8859-1&#xD;&#xA;" />
        </response>
        <sessionCookies>
          <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="True" expires="01/01/0001 00:00:00" />
          <cookie name="AltoroAccounts" value="ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=" path="/altoroj" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        </sessionCookies>
      </request>
    </RecordedSessionRequests>
    <SessionVerifier>
      <Enable>True</Enable>
      <OutSession>False</OutSession>
      <Pattern Base64="True">PlNpZ24gT2ZmPA==</Pattern>
      <PatternType>Text</PatternType>
      <request scheme="http" host="localhost" path="/altoroj/bank/main.jsp" port="9080" method="GET" RequestEncoding="28591" SessionRequestType="Regular" ordinal="14" ValidationStatus="None" MultiStepTested="true" sequencePlaybackRequired="true">
        <raw encoding="none">GET /altoroj/bank/main.jsp HTTP/1.1
Sec-Fetch-Site: same-origin
User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36
Referer: http://localhost:9080/altoroj/login.jsp
Cookie: JSESSIONID=C7DC9861991F70ADA043E95F8CFDE664; AltoroAccounts=ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=
Connection: keep-alive
Sec-Fetch-Mode: navigate
Upgrade-Insecure-Requests: 1
Host: localhost:9080
Cache-Control: max-age=0
Sec-Fetch-User: ?1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Origin: http://localhost:9080
Accept-Language: en-US
Sec-Fetch-Dest: document

</raw>
        <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj/bank" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        <cookie name="AltoroAccounts" value="ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=" path="/altoroj/bank" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        <response status="200" bodyEncoding="iso-8859-1">
          <body value="UEsDBBQAAAAIAJySNVNsLgjv5AgAAEUXAAAEACQAZGF0YQoAIAAAAAAAAQAYAKfAn1HnrtcBp8CfUeeu1wGnwJ9R567XAa1Ya3PbNhb97MzkP9xwtk13xhL1sFPbkdRxZCVRx7Y8ltxsPnlAEhIRgwQLgJa10x+/Fw9alCxnnd0qiQKQwH2fiwO9fvX6FeBf/Lx2w9evem8aDfgw+jS+hM+j07PRNTQaA/P4bDKcfb0aQaozDlc3H87HQwgaYfilOwzDs9kZ/Ovz7OIc2s0WzCTJFdNM5ISH4egygCDVujgJw+Vy2Vx2m0Iuwtl1+GBktc1mP2zo2s5mopNgYG2yOh8ynqv+Dknt4+NjJyAwi044yRf9gOYB2N3uTy+lJMH5Xk8zzenglGshBVyUuiS8F7qHJha9jGoCRkuD/lmy+34wFLmmuW7MVgUNIHazfqDpgw6N1vcQp0QqqvtMicbR0eFxox1A6KRxlt9BKum8H4TE6vwWKr3itBkrFYCkvB/YuUop1QFoVOJl2wVGTC/0xvcikazALu8HGZELlje0KE4Oi4f3PlQJuweWYJhwB5XB1uJIaC0yux6WLNHpCRwf/2T3oq1zITO7eS6zKSUyTgPAaKQCHy2McSQ2qal7Ylc1v6nCiNhDGZpEnDrZ/aDdav0UQCQkmtIPWhg8ynlBkoSZDPm5Kkjs50aGFSL9yHx6OgEplrgMNXeCQY9YGz9joOQ5RhdjvRVflif0wRnVY9kClIzrbzOyoCrkYiGaCzYPvLGdoy6klC1S3T9qhYNeSPCftiXjbEqAcLZAG6RZFMC9n2ICXPzcOmfducBwG+ueGGf0ltpbN8diqlJkxo2lteAEY8YTrCvBhTzBIkkwRVPUBpP5vBealdZA+Au2o9F9Phq/VaXLEGMJvTVTzGgTazgYDN0EbtQzgg+eCJ5TmkQkvnO+fPSzajsnEeWAJYURetC+nAbu/15o3w5c7/FpZnlR6lr9B5CTDMd/llSuAmuMH5I4pkrd0VU/mFZA2ylGlVHGXKpKnH4SbrVLVJVcO5Rbpbcz4z5RxsuFFGWeNGwtnZSS//J2u74WkiQMo938Vize/vO9r/qTFkLPgdEOv1ehDsK3BYuNDHSbY9epyrV7eFCV6ztbrk+8sSODRtv1QgNu20uwQ7hW4aC6BcEN6Lq2YMVVSPSvO4cI7JgThR050hBJiCL0peo+n63pbe+deTI2PiFUd3pazG+5wEKqwbHdqdxrH1SBv6dSs5jwhk2LAYnpZu99ZKY0LiUFi7wgHMDPeaSK91Udn8YxZkzvhGRE8rswIyy3lVy5NRdxiQ14cPEVTofDyc3lzPUEEz8f7u8EJY7h2bism5gxp3q2pfe/ohhxqew5afALg6vR9XRyeXr+dxnZ3WFk94eNjErFcgSrN3J6cXp+Dh9upuPL0XT6P5j61M6DHXYe/LCdriW6Vji+nI7PRnB6PptcT+DiZnazM6oeZ2uA2LGhT6PLsw3ytHEwLyUpitrJvHUM71kJF6OLD7h7Nhl6LublwCOdeRF47RYf0/pxVQ+qS/66hfbwiTcNuQttpP5Eatt2FW6sHIzhy+nlDO2EZrPZC6P625JXakxoIyKDjUZtuVGVugual9850DcBOviD0SV4QMO0zLCfrlyCUOLLdHR267D807GcmqprGmONOFrrXqofVPf0VF6rm1PpdM38DD7i0VLXsIfU4gclK439VD3KTShM7ZMtqfAy85+e/VaJPY4fCqI9A3SHO1zSpYJT06qR1f5goA53a0IQY6dn/6ZO0bCawpRpbPlI+Es8SHbo6oUlt+eiB4ID7zbGDGAtwvaeIkVwxzzXvc/hxYjEtBhYV72GPwvqvfq/XtoefEbgCvhdpDlMkaKk1dGNrx6XFf5A/0J5LDIKWsDGrQUmuUFo0y4Oi/VGy+MdeUrwMsO4eobHq1QsPY6cmb6trBl7xSlm19t819FzjNcmHM+cwpMaIdmgUpzOvS4jVlFOkXQqTCV2rYrxcaa0l6cc8dt44nbv9URhvKjI3VELPwjrgRvAlNxjV8T6c8tM1nds6FYbujBMaXyHW57fcXDYPW4ddVrd4+7xu87REe7dfgRDJOtMw5DIpCbokXM6h+u3m1181bgc6fwTrZx+pLC449MEv2oZ+C6PraL/WMaGe5iR1XEba966dd/+lnt7gW32tpAiE7iyfps7PG5VhdEaGPkm9720Y64OSHZ1yYltjW9MGXfcOWktqtZ+FSWk5B7Li9IcCkkbeBJKcU8Tc00AtMmX9ye8/MAfTBHUrFMgELugcobRATGHf7RNyt48VTHkLL4zlM82kbcon69My3iLgJO2PxgUuce13Z4nY3owMru49F6NSq+5tIebZ9T1/vJIsrd/U/k4mcw2aIFTsBb/yBPmQujnaYJVtnVDe9o7d5BGye5JvHJE58pN4Epg2OwJ6gQ75uy+/6pPdut99wK9ypBzpr3iqZ/hoUQ0zXDB36hcocxS3cYGztW5JPHqYJWVyuH8BfrgxQqXZLGg0nttf4caXI+mMzi9Gr9cz8+xKFb+UafVaW/2+n0Y53Hzkc/VTp2EKRyxjG7wq1lKvYDfYUkjZY5JpqAoI2ylKSIuWsH4wwUMhSyEtMC1INS4TwnEfFHiC0URbWuhCc0Q39oszxd2KZ3PsZ2xe2o4vkGmkYmYTspYK2A5btFmAS5HKyzw8AJntK2l3pc8p5JEjDPNqMI+kDyanFAjXzXRH7S+8iIXGpuCpHgEGnZgpJtXTWQDGeNEWjn7aw1sjkJX+wb6OmUygYJILL5HO1FjiL57pTiX1n/KV9g3WR4jfc01XrQ2rFiLNz0MlyQQEPRZBbZrCWzqSyKR0qEmYQ0AtDTZB4pvsRrpA3ZAEzQJLMOw0KRpo5cIWpNtXVWqxOPfSJBM3Zmwom0uZ+jSSpQSSpsq456q/GjCR5SdCXSG5aZj2R21sBScEty2EEbKdpn/WpX5+vfWRvug02RR1kQ2Eiox1+gfDasohqUKaR7iEYYJpgshV+H0y7iNl5LB/yvCgKhp7iXhwH1XODCfIcLG/j7jEQSdVutoHwyE9rdLfB8xwcGuVhhDZfpC0qxat+vZtR9IQvNzq/v9FUFd8UZDFtdtfO8/UEsBAi0AFAAAAAgAnJI1U2wuCO/kCAAARRcAAAQAJAAAAAAAAAAAAAAAAAAAAGRhdGEKACAAAAAAAAEAGACnwJ9R567XAafAn1HnrtcBp8CfUeeu1wFQSwUGAAAAAAEAAQBWAAAAKgkAAAAA" compressedBinaryValue="true" />
          <headers value="HTTP/1.1 200 &#xA;Content-Length: 5957&#xD;&#xA;Date: Tue, 21 Sep 2021 12:50:53 GMT&#xD;&#xA;Content-Type: text/html;charset=ISO-8859-1&#xD;&#xA;" />
        </response>
        <sessionCookies>
          <cookie name="JSESSIONID" value="C7DC9861991F70ADA043E95F8CFDE664" path="/altoroj" domain="localhost" secure="False" httpOnly="True" expires="01/01/0001 00:00:00" />
          <cookie name="AltoroAccounts" value="ODAwMDAyflNhdmluZ3N+NzY1NS40Mnw4MDAwMDN+Q2hlY2tpbmd+MjQ2NjgzNS4zOXw0NTM5MDgyMDM5Mzk2Mjg4fkNyZWRpdCBDYXJkfjEwMC40Mnw=" path="/altoroj" domain="localhost" secure="False" httpOnly="False" expires="01/01/0001 00:00:00" />
        </sessionCookies>
      </request>
    </SessionVerifier>
    <InSessionRequestIndex>3</InSessionRequestIndex>
    <ActionBasedSequence RecordingBrowser="Chromium">
      <Enabled>True</Enabled>
      <UseAbl>True</UseAbl>
      <StartingUrl>http://localhost:9080/altoroj/</StartingUrl>
      <Actions>
        <Action ActionType="Wait" BrowserIndex="0" Validated="Success" ID="6e93db9b-e517-4233-b484-3f3a24db48b2">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <tagName name="A" />
              <attributes>
                <attribute key="NAME" value="wait(sec)" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <Value>0</Value>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Click" BrowserIndex="0" Validated="Success" ID="1ce6c1af-ba57-4679-bcaa-f91797819de8">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <hybridXPath>//*[@id='LoginLink']/FONT[1]</hybridXPath>
              <xPath>//HTML[1]/BODY[1]/DIV[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/A[1]/FONT[1]</xPath>
              <tagName name="FONT" />
              <innerText>Sign In</innerText>
              <parentForm>&lt;FORM  id="frmSearch" method="get" action="/altoroj/search.jsp" /&gt;</parentForm>
              <attributes>
                <attribute key="style" value="font-weight: bold; color: red;" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Wait" BrowserIndex="0" Validated="Success" ID="c439b299-6e74-44b2-82bc-9dcf9926d27a">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <tagName name="A" />
              <attributes>
                <attribute key="NAME" value="wait(sec)" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <Value>0</Value>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Click" BrowserIndex="0" Validated="Success" ID="380d7c1a-2818-4ef1-b2e6-163d2fa9f8ec">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <hybridXPath>//*[@id='uid']</hybridXPath>
              <xPath>//HTML[1]/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/DIV[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/INPUT[1]</xPath>
              <tagName name="INPUT" />
              <parentForm>&lt;FORM  action="doLogin" method="post" name="login" id="login" onsubmit="return (confirminput(login));" /&gt;</parentForm>
              <attributes>
                <attribute key="type" value="text" />
                <attribute key="id" value="uid" />
                <attribute key="name" value="uid" />
                <attribute key="value" value="" />
                <attribute key="style" value="width: 150px;" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Set" BrowserIndex="0" Validated="Success" ID="54eb21b2-c11e-4986-b12e-67e1e307e7e5">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <hybridXPath>//*[@id='uid']</hybridXPath>
              <xPath>//HTML[1]/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/DIV[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/INPUT[1]</xPath>
              <tagName name="INPUT" />
              <parentForm>&lt;FORM  action="doLogin" method="post" name="login" id="login" onsubmit="return (confirminput(login));" /&gt;</parentForm>
              <attributes>
                <attribute key="type" value="text" />
                <attribute key="id" value="uid" />
                <attribute key="name" value="uid" />
                <attribute key="value" value="j" />
                <attribute key="style" value="width: 150px;" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <Value Base64="true" Encrypted="true">KMB7RDslbF8zxCu7z0DOzA==</Value>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Wait" BrowserIndex="0" Validated="Success" ID="008e2e06-087d-4573-a79e-8576c5a2f175">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <tagName name="A" />
              <attributes>
                <attribute key="NAME" value="wait(sec)" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <Value>0</Value>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Click" BrowserIndex="0" Validated="Success" ID="8460c448-e01d-448e-8bd0-501dad3c9284">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <hybridXPath>//*[@id='passw']</hybridXPath>
              <xPath>//HTML[1]/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/DIV[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/INPUT[1]</xPath>
              <tagName name="INPUT" />
              <parentForm>&lt;FORM  action="doLogin" method="post" name="login" id="login" onsubmit="return (confirminput(login));" /&gt;</parentForm>
              <attributes>
                <attribute key="type" value="password" />
                <attribute key="id" value="passw" />
                <attribute key="name" value="passw" />
                <attribute key="style" value="width: 150px;" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Set" BrowserIndex="0" Validated="Success" ID="696b8569-51d3-4fc6-82d0-633b44fd20ee">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <hybridXPath>//*[@id='passw']</hybridXPath>
              <xPath>//HTML[1]/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/DIV[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/INPUT[1]</xPath>
              <tagName name="INPUT" />
              <parentForm>&lt;FORM  action="doLogin" method="post" name="login" id="login" onsubmit="return (confirminput(login));" /&gt;</parentForm>
              <attributes>
                <attribute key="type" value="password" />
                <attribute key="id" value="passw" />
                <attribute key="name" value="passw" />
                <attribute key="style" value="width: 150px;" />
                <attribute key="value" value="d" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <Value Base64="true" Encrypted="true">4WxTLKB64EcfPdrD5dIocQ==</Value>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Wait" BrowserIndex="0" Validated="Success" ID="e79fc03c-02f7-48c2-8a68-bd80670bc7d8">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <tagName name="A" />
              <attributes>
                <attribute key="NAME" value="wait(sec)" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <Value>0</Value>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
        <Action ActionType="Click" BrowserIndex="0" Validated="Success" ID="32740262-9f37-4a31-b87d-db5ef0ffc99c">
          <ElementLocations>
            <ElementLocation isPreferred="False">
              <hybridXPath>//*[@id='login']/TABLE[1]/TBODY[1]/TR[3]/TD[2]/INPUT[1]</hybridXPath>
              <xPath>//HTML[1]/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[2]/DIV[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[3]/TD[2]/INPUT[1]</xPath>
              <tagName name="INPUT" />
              <parentForm>&lt;FORM  action="doLogin" method="post" name="login" id="login" onsubmit="return (confirminput(login));" /&gt;</parentForm>
              <attributes>
                <attribute key="type" value="submit" />
                <attribute key="name" value="btnSubmit" />
                <attribute key="value" value="Login" />
              </attributes>
            </ElementLocation>
          </ElementLocations>
          <ProxyOrdinalRequestBeforeAction>-1</ProxyOrdinalRequestBeforeAction>
        </Action>
      </Actions>
      <VerifyElementsActionThreshold>0.6</VerifyElementsActionThreshold>
      <LogoutRegex>log[_\-\s]?out|sign[_\-\s]?out|log[_\-\s]?off|sign[_\-\s]?off|exit|quit|bye-bye|clearuser|invalidate|sign out|sign off|log out|log off|disconnect</LogoutRegex>
    </ActionBasedSequence>
    <VariablesDefinitions>
      <VariableDefinition IsRegularExpression="False" Name="">
        <VariableType>DefaultDefinitions</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="^BV_">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="^CFID">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="^CFTOKEN">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__VIEWSTATE">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__EVENTVALIDATION">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__REQUESTDIGEST">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__VIEWSTATEGENERATOR">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__EVENTARGUMENT">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>None</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__EVENTTARGET">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>None</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__VIEWSTATEID">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments>An id of the viewstate that is stored in the server's db. </Comments>
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__LASTFOCUS">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__SCROLLPOSITIONX">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__SCROLLPOSITIONY">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__PREVIOUSPAGE">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__CALLBACKID">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>None</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__CALLBACKPARAM">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>None</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="__VIEWSTATEFIELDCOUNT">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="__VIEWSTATE\d+">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path>/</Path>
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="wsdl">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="disco">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="javax.faces.viewstate">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="^BV_">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="JSESSIONID">
        <VariableType>Custom</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="IIS_COOKIELESS">
        <VariableType>Custom</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="ses|token">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments>Session cookie regular expression</Comments>
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="(?:server|user|u)_*id">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments>Server or user id</Comments>
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="JSESSIONID">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="PHPSESSID">
        <VariableType>Parameter</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="__utm.|vgnvisitor|_csuid|_csoot|WEBTRENDS_ID|WT_FPS|cookieenabledcheck|__qc[ab]|MintUnique|PD_STATEFUL|_sn|BCSI\\-">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments>Cookie that tracks visitor activity for a third-party application</Comments>
        <RequestIgnoreStatus>Full</RequestIgnoreStatus>
        <EntityIgnoreStatus>Full</EntityIgnoreStatus>
        <ExcludeFromTest>True</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="(ASPSESSIONID[a-zA-Z0-9]{8})">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>True</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="WC_AUTHENTICATION_(\d+)">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>True</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="True" Name="WC_USERACTIVITY_(\d+)">
        <VariableType>Cookie</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>None</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>True</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="GUID">
        <VariableType>Custom</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="NUMERIC">
        <VariableType>Custom</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="HEXDECIMAL">
        <VariableType>Custom</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="DATE">
        <VariableType>Custom</VariableType>
        <Hosts />
        <Path />
        <Comments />
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>False</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>TemplateDefined</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value />
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="JSESSIONID">
        <VariableType>Cookie</VariableType>
        <Hosts>demo.testfire.net</Hosts>
        <Path>/</Path>
        <Comments>Extracted from manual login sequence recording</Comments>
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>Login</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value>B8D5B97F603D25957875F150EF2BDBC6</Value>
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="AltoroAccounts">
        <VariableType>Cookie</VariableType>
        <Hosts>demo.testfire.net</Hosts>
        <Path>/</Path>
        <Comments>Extracted from manual login sequence recording</Comments>
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>Login</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value>"ODAwMDAyflNhdmluZ3N+Njg4MTQ0LjgzOTk5OTk5OTl8ODAwMDAzfkNoZWNraW5nfjEuMzAyOTE0MTg0ODU4MDQ5RTIxfDQ1MzkwODIwMzkzOTYyODh+Q3JlZGl0IENhcmR+LTQzNDYxOS4xNnw="</Value>
        </SessionID>
      </VariableDefinition>
      <VariableDefinition IsRegularExpression="False" Name="JSESSIONID">
        <VariableType>Cookie</VariableType>
        <Hosts>localhost</Hosts>
        <Path>/altoroj</Path>
        <Comments>Extracted from manual login sequence recording</Comments>
        <RequestIgnoreStatus>Value</RequestIgnoreStatus>
        <EntityIgnoreStatus>Value</EntityIgnoreStatus>
        <ExcludeFromTest>False</ExcludeFromTest>
        <SessionIDEnabled>True</SessionIDEnabled>
        <CaptureName />
        <CaptureIndex>-1</CaptureIndex>
        <VariableOrigin>Login</VariableOrigin>
        <AlwaysSend>False</AlwaysSend>
        <IsGroup>False</IsGroup>
        <SessionID TrackingMethod="ExploreAndLogin">
          <Value>C7DC9861991F70ADA043E95F8CFDE664</Value>
        </SessionID>
      </VariableDefinition>
    </VariablesDefinitions>
    <CustomParameters>
      <CustomParameter LogicalName="JSESSIONID">
        <Pattern>;(?:JSESSIONID|jsessionid)=([^/]+)$</Pattern>
        <NameGroupIndex>-1</NameGroupIndex>
        <ValueGroupIndex>1</ValueGroupIndex>
        <TargetSegment>Path</TargetSegment>
        <ResponsePattern />
        <Condition />
      </CustomParameter>
      <CustomParameter LogicalName="IIS_COOKIELESS">
        <Pattern>(\((?:[ASF]\([a-zA-Z0-9]+\)){1,3}\))</Pattern>
        <NameGroupIndex>-1</NameGroupIndex>
        <ValueGroupIndex>1</ValueGroupIndex>
        <TargetSegment>Path</TargetSegment>
        <ResponsePattern />
        <Condition />
      </CustomParameter>
      <CustomParameter LogicalName="GUID">
        <Pattern>((\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1})</Pattern>
        <NameGroupIndex>-1</NameGroupIndex>
        <ValueGroupIndex>1</ValueGroupIndex>
        <TargetSegment>Path</TargetSegment>
        <ResponsePattern />
        <Condition />
      </CustomParameter>
      <CustomParameter LogicalName="HEXDECIMAL">
        <Pattern>(([A-Fa-f0-9]{40})|([A-Fa-f0-9]{32}))</Pattern>
        <NameGroupIndex>-1</NameGroupIndex>
        <ValueGroupIndex>1</ValueGroupIndex>
        <TargetSegment>Path</TargetSegment>
        <ResponsePattern />
        <Condition />
      </CustomParameter>
      <CustomParameter LogicalName="DATE">
        <Pattern>\b((19|20)\d\d[-/.](0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01]))\b</Pattern>
        <NameGroupIndex>-1</NameGroupIndex>
        <ValueGroupIndex>1</ValueGroupIndex>
        <TargetSegment>Path</TargetSegment>
        <ResponsePattern />
        <Condition />
      </CustomParameter>
      <CustomParameter LogicalName="NUMERIC">
        <Pattern>\b(\d{8,128})\b</Pattern>
        <NameGroupIndex>-1</NameGroupIndex>
        <ValueGroupIndex>1</ValueGroupIndex>
        <TargetSegment>Path</TargetSegment>
        <ResponsePattern />
        <Condition />
      </CustomParameter>
    </CustomParameters>
  </SessionManagement>
  <customHeaders />
  <UserInput>
    <FormFiller Version="2.0" Enabled="True" DefaultValue="1234" UseDefaultValue="True" RandomDefaultValue="False">
      <Group LogicalName="InternalAppScanUserName" MatchType="Partial" Action="">
        <Name>InternalAppScanUserName</Name>
        <Selection>Ignore</Selection>
        <Value>jsmith</Value>
        <MatchNames>
          <MatchName>uid</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="InternalAppScanPassword" MatchType="Partial" Action="">
        <Name>InternalAppScanPassword</Name>
        <Selection>Ignore</Selection>
        <Value Encrypt="true">rVzDVLnUnuV5cvLTYYDv3A==</Value>
        <MatchNames>
          <MatchName>passw</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Address" MatchType="Partial" Action="">
        <Name>Address</Name>
        <Selection>Ignore</Selection>
        <Value>753 Main Street</Value>
        <MatchNames>
          <MatchName>addr</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Age" MatchType="Partial" Action="">
        <Name>Age</Name>
        <Selection>Ignore</Selection>
        <Value>25</Value>
        <MatchNames>
          <MatchName>age</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Areacode" MatchType="Partial" Action="">
        <Name>Area code</Name>
        <Selection>Ignore</Selection>
        <Value>555</Value>
        <MatchNames>
          <MatchName>area</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="City" MatchType="Partial" Action="">
        <Name>City</Name>
        <Selection>Ignore</Selection>
        <Value>Mystery</Value>
        <MatchNames>
          <MatchName>city</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Company" MatchType="Partial" Action="">
        <Name>Company</Name>
        <Selection>Ignore</Selection>
        <Value>Acme-Hackme Corp.</Value>
        <MatchNames>
          <MatchName>company</MatchName>
          <MatchName>firm</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Country" MatchType="Partial" Action="">
        <Name>Country</Name>
        <Selection>Ignore</Selection>
        <Value>USA</Value>
        <MatchNames>
          <MatchName>country</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Email" MatchType="Partial" Action="">
        <Name>Email</Name>
        <Selection>Ignore</Selection>
        <Value>test@altoromutual.com</Value>
        <MatchNames>
          <MatchName>mail</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="MailTo" MatchType="Complete" Action="">
        <Name>MailTo</Name>
        <Selection>Ignore</Selection>
        <Value>test@altoromutual.com</Value>
        <MatchNames>
          <MatchName>To</MatchName>
          <MatchName>to</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="MailFrom" MatchType="Complete" Action="">
        <Name>MailFrom</Name>
        <Selection>Ignore</Selection>
        <Value>test@altoromutual.com</Value>
        <MatchNames>
          <MatchName>From</MatchName>
          <MatchName>from</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="MailBcc" MatchType="Complete" Action="">
        <Name>MailBcc</Name>
        <Selection>Ignore</Selection>
        <Value>test@altoromutual.com</Value>
        <MatchNames>
          <MatchName>Bcc</MatchName>
          <MatchName>bcc</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Day" MatchType="Partial" Action="">
        <Name>Day</Name>
        <Selection>Ignore</Selection>
        <Value>01</Value>
        <MatchNames>
          <MatchName>day</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Month" MatchType="Partial" Action="">
        <Name>Month</Name>
        <Selection>Ignore</Selection>
        <Value>01</Value>
        <MatchNames>
          <MatchName>month</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Number" MatchType="Partial" Action="">
        <Name>Number</Name>
        <Selection>Ignore</Selection>
        <Value>9876543210</Value>
        <MatchNames>
          <MatchName>num</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Passport" MatchType="Complete" Action="">
        <Name>Passport</Name>
        <Selection>Ignore</Selection>
        <Value>9876543210</Value>
        <MatchNames>
          <MatchName>passport</MatchName>
          <MatchName>Passport</MatchName>
          <MatchName>PASSPORT</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Phone" MatchType="Partial" Action="">
        <Name>Phone</Name>
        <Selection>Ignore</Selection>
        <Value>555-555-5555</Value>
        <MatchNames>
          <MatchName>phone</MatchName>
          <MatchName>tel</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="SocialSecurity" MatchType="Partial" Action="">
        <Name>Social Security</Name>
        <Selection>Ignore</Selection>
        <Value>987 65 4321</Value>
        <MatchNames>
          <MatchName>ssn</MatchName>
          <MatchName>social</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="State" MatchType="Partial" Action="">
        <Name>State</Name>
        <Selection>Ignore</Selection>
        <Value>AK</Value>
        <MatchNames>
          <MatchName>state</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Year" MatchType="Partial" Action="">
        <Name>Year</Name>
        <Selection>Ignore</Selection>
        <Value>09</Value>
        <MatchNames>
          <MatchName>year</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="ZipCode" MatchType="Partial" Action="">
        <Name>Zip Code</Name>
        <Selection>Ignore</Selection>
        <Value>99801</Value>
        <MatchNames>
          <MatchName>zip</MatchName>
          <MatchName>postal</MatchName>
        </MatchNames>
      </Group>
      <Group LogicalName="Date" MatchType="Partial" Action="">
        <Name>Date</Name>
        <Selection>Ignore</Selection>
        <Value>2019-01-01</Value>
        <MatchNames>
          <MatchName>date</MatchName>
        </MatchNames>
      </Group>
    </FormFiller>
    <PlatformAuthentication>
      <Enabled>False</Enabled>
      <Domain />
      <Password Encrypted="false" />
      <UserName />
    </PlatformAuthentication>
    <ClientCertificateOption>
      <Option>2</Option>
    </ClientCertificateOption>
    <ClientSideCertificates>
      <ClientSideCertificate>
        <Enabled>False</Enabled>
        <FilePath />
        <Raw />
        <KeyPath />
        <Password Encrypted="false" />
        <InOldPemFormat>False</InOldPemFormat>
      </ClientSideCertificate>
    </ClientSideCertificates>
    <UserStoreCertificates />
  </UserInput>
</ScanConfiguration>